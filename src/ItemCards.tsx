import React from 'react';
import ListGroup from 'react-bootstrap/ListGroup';
import Button from 'react-bootstrap/Button';

export interface ItemCard {
    id: number;
    name: string;
    nodeId: string;
}

interface ItemCardFormProps {
    editable: boolean;
    fireUpdate?: boolean;
    modal?: (showModal: boolean, onHide: (() => void), updateItems: (() => Promise<void>)) => any;
    // addItem: () => void;
    deleteItem?: (nodeId: string) => void;
    loadItems: () => Promise<ItemCard[]>;
}

interface ItemCardFormState {
    error: Error|null;
    isLoaded: boolean;
    items: ItemCard[];
    showModal: boolean;
}

export class ItemCardForm extends React.Component<ItemCardFormProps, ItemCardFormState> {

    state = {
        error: null,
        isLoaded: false,
        items: [],
        showModal: false
    }

    render() {
        const { error, items } = this.state;
        const { editable, modal, deleteItem } = this.props;
        const onHide = () => { this.setState({showModal: false}); };
        if (error) {
            return <div>{(error as any).message}</div>;
        } else {
            return (
                <>
                <ListGroup horizontal>
                {items.map((item: ItemCard) => ( 
                    <ListGroup.Item 
                        key={`item_${item.id}`}
                    >{item.name}
                    {editable ? <button type="button" onClick={async () => { await (deleteItem as (nodeId: string) => void)(item.nodeId); this.updateItems(); } } className="close"><span aria-hidden="true">×</span></button> : null}
                    </ListGroup.Item> 
                    ))}
                {editable ? <Button variant="light" onClick={() => this.setState({showModal: true})}>+</Button> : null}
                </ListGroup>
                {modal ? (modal as (showModal: boolean, onHide: (() => void), updateItems: (() => Promise<void>)) => any)(
                    this.state.showModal, 
                    onHide, 
                    this.updateItems)
                : null}
                </>
            );
        }
    }

    updateItems = async () => {
        this.props.loadItems().then(items => {
            this.setState({ items, isLoaded: true });
        });
    }

    componentDidMount() {
        this.updateItems();
    }

    componentDidUpdate(prevProps: any, prevState: any) {
        if (this.props === prevProps) {
            return;
        }
        this.updateItems();
    }
}

export default ItemCardForm;
