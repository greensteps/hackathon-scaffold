import React from 'react';
import { createEvent, createEventParticipant, deleteEvent, deleteEventParticipantByNodeId, Event, EventParticipant, loadEvents, 
    loadEventParticipants, loadUsers, renameEvent, User } from './dataService';
import Dropdown from 'react-bootstrap/Dropdown';
import Modal from 'react-bootstrap/Modal';
import ListGroup from 'react-bootstrap/ListGroup';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import { ItemCardForm } from './ItemCards';

interface EventProps {
}

interface EventState {
    events: Event[];
    isLoaded: boolean;
    fireParticipantUpdate: boolean;
    currentEventId: number;
    currentEventName: string;
    currentEventDescription: string;
    validated: boolean;
    users: User[];
}

export class EventSelect extends React.Component<EventProps, EventState> {

    state = {
        events: [],
        isLoaded: false,
        fireParticipantUpdate: false,
        currentEventId: 0,
        currentEventName: "Select event",
        currentEventDescription: "",
        users: [],
        validated: false
    }

    loadEvents = async () => {
        let users = this.state.users as User[];
        if (users.length === 0) {
            users = await loadUsers();
        }
        const events = await loadEvents();
        this.setState( { 
            events,
            isLoaded: true,
            users,
            validated: false
        });
        console.log("Events", this.state.events);
    }

    handleSubmit = async (event: any) => {
        const modifiedEventName = (document.getElementById("eventNameInput") as any).value;
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
            return;
        }
        event.preventDefault();
        console.log(`Submit! ${this.state.currentEventName} -> ${(document.getElementById("eventNameInput") as any).value}`);
        await renameEvent(this.state.currentEventId, modifiedEventName);
        this.setState({ validated: true });
        form.submit();
    }

    componentDidMount() {
        this.loadEvents();
    };

    onSelect = (eventKey: any, event: object) => {
        const currentEventItem = (this.state.events as any[]).find(event => event.id === parseInt(eventKey));
        console.log("Loading event", eventKey, currentEventItem);
        this.setState({ 
            currentEventId: parseInt(eventKey),
            currentEventName: currentEventItem.name,
            currentEventDescription: currentEventItem.description,
            fireParticipantUpdate: !this.state.fireParticipantUpdate
        });
    }

    createEvent = async () => {
        console.log("Request for event creation");
        const event = await createEvent();
        await this.loadEvents();
        this.setState({ 
            currentEventId: event.id,
            currentEventName: event.name,
            currentEventDescription: event.description,
        });
    }

    deleteEvent = async () => {
        if (this.state.currentEventId === 0) {
            return;
        }
        console.log("Request for event suppression");
        await deleteEvent(this.state.currentEventId);
        await this.setState({
            isLoaded: false,
            currentEventId: 0,
            currentEventName: "Select event",
            currentEventDescription: "",
        });
        await this.loadEvents();
    }

    render() {
        const { currentEventId, currentEventName, events, validated } = this.state;
        let eventForm = null;
        if (currentEventId !== 0) {
                eventForm = (
                    <Form noValidate validated={validated} onSubmit={this.handleSubmit}>
                    <Form.Row>
                    <Form.Group as={Col} md="4" >
                    <Form.Label>{currentEventName}</Form.Label>
                    <Form.Control
                    required
                    placeholder={currentEventName}
                    name="eventName"
                    id="eventNameInput"
                    type="text"
                    />
                    <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                    </Form.Group>
                    </Form.Row>
                    <Form.Row>
                    <Form.Label>Participants</Form.Label>
                    <ItemCardForm
                        editable={true}
                        fireUpdate={this.state.fireParticipantUpdate}
                        deleteItem={async (nodeId) => {
                            console.log(`Request to delete user: ${nodeId}`);
                            await deleteEventParticipantByNodeId(nodeId);
                        }}
                        loadItems={async () => {
                            const allParticipants = await loadEventParticipants();
                            const participants = allParticipants.filter(participant => {
                                console.log("Participants", participant, this.state.currentEventId);
                                return participant.eventId === this.state.currentEventId;
                            });
                            const itemCards = participants.map((participant: EventParticipant) => {
                                return {
                                    id: participant.id,
                                    name: participant.name,
                                    nodeId: participant.nodeId
                                };
                            });
                            return itemCards;
                        }}
                        modal={
                            (showModal, onHide, updateItems) => {
                                return <Modal show={showModal} onHide={onHide}>
                                    <Modal.Dialog >
                                    <ListGroup>
                                    {this.state.users.map((user: User) => ( 
                                        <ListGroup.Item 
                                            key={`user_${user.id}`}
                                            onClick={async () => {
                                                console.log("Click on user", user.name);
                                                await createEventParticipant(this.state.currentEventId, user.id);
                                                await updateItems();
                                                onHide();
                                            }}
                                        >{user.name}
                                        </ListGroup.Item> 
                                        ))}
                                    </ListGroup>
                                    </Modal.Dialog>
                                    </Modal>
                            }
                        }
                    />
                    </Form.Row>
                    <Button type="submit">Submit form</Button>
                    </Form>
                    );
        }
        return (
            <div className="eventSelect">
                <table className="App-header">
                    <thead>
                    <tr>
                    <th>
                    <Dropdown onSelect={this.onSelect}>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                    {this.state.currentEventName}
                    </Dropdown.Toggle>  
                    <Dropdown.Menu>
                    {events.map((item: Event) => ( 
                        <Dropdown.Item 
                            id={`event_${item.id}`} 
                            key={`event_${item.id}`}
                            eventKey={`${item.id}`} >{item.name}
                        </Dropdown.Item> 
                        ))}
                    </Dropdown.Menu>
                    </Dropdown>
                    </th>
                    <th>
                    <Button variant="success" onClick={this.createEvent}>+</Button>
                    </th>
                    <th>
                    <Button variant="danger" onClick={this.deleteEvent}>-</Button>
                    </th>
                    </tr>
                    </thead>
                </table>
                {eventForm}
            </div>
        );
    }
}

export default EventSelect;