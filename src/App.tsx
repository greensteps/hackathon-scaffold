import React from 'react';
// import { useState } from 'react';
import './App.css';
import EventSelect from './Event';
import UserForm from './UserForm';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

function App() {
    return (
        <div className="App">
        <Tabs>
        <TabList>
          <Tab>Events</Tab>
          <Tab>Users</Tab>
        </TabList>
        <TabPanel>
          <EventSelect /> 
        </TabPanel>
        <TabPanel>
          <UserForm />
        </TabPanel>
        </Tabs>
        </div>
        );
}

export default App;
