import axios from 'axios';

export interface QueryItem {
    name: string;
    query?: string;
    queryFunction?: (...params: any[]) => string;
}

export const queryBucket = {
    "events": {
        name: "allEvents",
        query: "query MyQuery { allEvents { edges { node { id name description } } }}"
    },
    "users": {
        name: "allUsers",
        query: "query MyQuery { allUsers { edges { node { id fullName nodeId } } }}"
    },
    "participants": {
        name: "allEventParticipants",
        query: "query MyQuery { allEventParticipants { edges { node { id eventId userId nodeId } } }}"
    },
    "mutationEventName": {
        name: "MyQuery",
        queryFunction: function (id: number, name: string): string {
            return `mutation MyQuery { __typename updateEventById(input: {eventPatch: {name: "${name}"}, id: ${id}}) { clientMutationId }}`;
        }
    },
    "createEvent": {
        name: "createEvent",
        queryFunction: function (): string { return 'mutation MyQuery { createEvent(input: {event: {name: "New event"}}) { clientMutationId event { id name description } }}' }
    },
    "createEventParticipant": {
        name: "createEventParticipant",
        queryFunction: function(eventId: number, userId: number): string {
            return `mutation MyQuery { createEventParticipant(input: {eventParticipant: {eventId: ${eventId}, userId: ${userId}}}) {  clientMutationId eventParticipant { userId nodeId id eventId } }}`
        }
    },
    "deleteEvent": {
        name: "MyQuery",
        queryFunction: function(eventId: number): string {
            return `mutation MyQuery { __typename deleteEventById(input: {id: ${eventId}}) { clientMutationId }}`;
        }
    },
    "deleteEventParticipant": {
        name: "MyQuery",
        queryFunction: function(eventParticipantId: number): string {
            return `mutation MyQuery { __typename deleteEventParticipant(input: {nodeId: "${eventParticipantId}"}) { clientMutationId }}`;
        }
    },
    "createUser": {
        name: "createUser",
        queryFunction: function (userName: string): string { 
            return `mutation MyQuery { createUser(input: {user: {fullName: "${userName}"}}) { clientMutationId user { id fullName nodeId } }}`;
        }
    },
    "deleteUser": {
        name: "MyQuery",
        queryFunction: function(userId: number): string {
            return `mutation MyQuery { __typename deleteUserById(input: {id: ${userId}}) { clientMutationId }}`;
        }
    }
}

export async function queryQl(queryIndex: QueryItem, variables: object): Promise<any[]> {
    try {
        const response = await axios.post('http://localhost:5000/graphql', {
            operationName: "MyQuery",
            query: queryIndex.query,
            variables
        });
        console.log("===================== queryQl", queryIndex.name, queryIndex.query);
        return response.data.data[queryIndex.name].edges;
    } catch (error) {
        // If there's an error, set the error to the state
        console.error(error);
        return [];
    }
}

export async function queryQlFunction(queryIndex: QueryItem, variables: object, ...params: any[]): Promise<any[]|any> {
    const computedQuery = (queryIndex.queryFunction as (...params: any[]) => string)(...params);
    console.log("===================== queryQlFunction", queryIndex.name, ...params, computedQuery);
    const response = await axios.post('http://localhost:5000/graphql', {
        operationName: "MyQuery",
        query: computedQuery,
        variables
    });
    console.log("==================== queryQlFunction response:", queryIndex.name, response);
    if (response.data.errors) {
        console.error("===================== queryQlFunction", queryIndex.name, response);
        throw response.data.errors;
    }
    return response.data.data[queryIndex.name];
}
