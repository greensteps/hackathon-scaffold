import { queryBucket, queryQl, queryQlFunction } from './graphql';

interface IdAndName {
    id: number;
    name: string;
    nodeId: string;
}

export interface User extends IdAndName {};

export interface Event extends IdAndName {
    description: string;
}

export interface EventParticipant extends IdAndName {
    userId: number;
    eventId: number;
}

function nameFromId(id: number, list: IdAndName[]): string {
    if (list.length === 0) {
        console.error("Careful! learning (item|path) are not loaded");
        return "";
    } 
    const lpId = list.find((e: IdAndName) => e.id === id);
    if (!lpId) {
        console.error("No learning (item|path) with id", id);
        return "";
    }
    return (lpId as IdAndName).name
}

/*************************************************************************************************************
*                                             Events                                                         *
**************************************************************************************************************/

export async function deleteEvent(eventId: number): Promise<void> {
    await queryQlFunction(queryBucket["deleteEvent"], {}, eventId);
}

export async function loadEvents(): Promise<Event[]> {
    const events = await queryQl(queryBucket["events"], {});
    return events.map(event => event.node);
}

export async function renameEvent(eventId: number, name: string): Promise<void> {
    await queryQlFunction(queryBucket["mutationEventName"], {}, eventId, name);
}

export async function createEvent(): Promise<Event> {
    const event = await queryQlFunction(queryBucket["createEvent"], {});
    console.log("Created new event", event.event);
    return event.event;
}

/*************************************************************************************************************
*                                             Users                                                          *
**************************************************************************************************************/

let users = [] as User[];

export async function loadUsers(): Promise<User[]> {
    const usersQl = await queryQl(queryBucket["users"], {});
    const userNodes = usersQl.map(e => { 
        e.node.name = e.node.fullName;
        return e.node;
    });
    users = userNodes;
    console.log("dataService loadUsers: ", users);
    return userNodes;
}

export async function deleteUser(userId: number): Promise<void> {
    const eventParticipants = (await loadEventParticipants()).filter(eventParticipant => eventParticipant.userId === userId);
    const deleteEventParticipantPromises = eventParticipants.map(item => {
        return queryQlFunction(queryBucket["deleteEventParticipant"], {}, item.nodeId);
    });
    await Promise.all(deleteEventParticipantPromises);
    await queryQlFunction(queryBucket["deleteUser"], {}, userId);
}

export async function createUser(userName: string): Promise<User> {
    const user = await queryQlFunction(queryBucket["createUser"], {}, userName);
    user.user.name = user.user.fullName;
    return user.user;
}

export function syncUserName(userId: number): string {
    return nameFromId(userId, users);
}

/*************************************************************************************************************
*                                             Event Participants                                             *
**************************************************************************************************************/

export async function loadEventParticipants(): Promise<EventParticipant[]> {
    const allEventParticipants = await queryQl(queryBucket["participants"], {});
    const participants = allEventParticipants.map(e => {
        e.node.name = syncUserName(e.node.userId);
        return e.node;
    });
    return participants;
}

export async function createEventParticipant(eventId: number, userId: number): Promise<EventParticipant> {
    const participant = await queryQlFunction(queryBucket["createEventParticipant"], {}, eventId, userId);
    console.log("Create participant", participant);
    return participant;
}

export async function deleteEventParticipantByNodeId(eventParticipantNodeId: string): Promise<void> {
    await queryQlFunction(queryBucket["deleteEventParticipant"], {}, eventParticipantNodeId);
}
