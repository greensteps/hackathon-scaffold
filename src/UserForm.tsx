import React from 'react';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import { createUser, deleteUser, loadUsers, User } from './dataService';

interface UserFormProps {
}

interface UserFormState {
    error: Error|null;
    isLoaded: boolean;
    users: User[];
    showModal: boolean;
    validated: boolean;
}

export class UserForm extends React.Component<UserFormProps, UserFormState> {

    state = {
        error: null,
        isLoaded: false,
        users: [],
        showModal: false,
        validated: false
    }

    createUser = async (event: any) => {
        const userName = (document.getElementById("userNameInput") as any).value;
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
            return;
        }
        event.preventDefault();
        const user = await createUser(userName);
        console.log("addUser", userName, user);
        this.setState({ validated: true, showModal: false });
        form.submit();
    }

    removeUser = async (userId: number) => {
        console.log("removeUser", userId);
        await deleteUser(userId);
        this.setState({ isLoaded: false });
        await this.loadUserItems();
    }

    render() {
        const { error, isLoaded, users } = this.state;
        if (error) {
            return <div>{(error as any).message}</div>;
        } else if (!isLoaded) {
            return null;
        } else {
            return (
                <>
                <div className="grid">
                    <Table striped bordered hover>
                    <thead>
                    <tr>
                    <th><Button variant="success" onClick={() => this.setState({ showModal: true})}>+</Button></th>
                    <th>#</th>
                    <th>Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    {users.map((item: User) => (
                        <tr key={`item_${item.name}`}>
                        <td><Button variant="danger" onClick={() => this.removeUser(item.id)}>-</Button></td>
                        <td>{item.id}</td>
                        <td>{item.name}</td>
                        </tr>
                        ))}
                    </tbody>
                    </Table>
                    <Modal show={this.state.showModal} onHide={() => this.setState({ showModal: false})}>
                    <Modal.Dialog >
                    <Form noValidate validated={this.state.validated} onSubmit={this.createUser}>
                    <Form.Row>
                    <Form.Control
                    required
                    placeholder="input user name"
                    name="userName"
                    id="userNameInput"
                    type="text"
                    />
                    <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                    </Form.Row>
                    <Button type="submit">Create user</Button>
                    </Form>
                    </Modal.Dialog>
                    </Modal>
                </div>
                </>
            );
        }
    }

    loadUserItems = async () => {
        try {
            const users = await loadUsers();
            // Log the response so we can look at it in the console
            console.log("users", users);

            this.setState(() => ({
                isLoaded: true,
                users
            }));
            console.log("state", this.state)
        } catch (error) {
            // If there's an error, set the error to the state
            this.setState(() => ({ error }))
        }
    }

    componentDidMount() {
        this.loadUserItems();
    }

    componentDidUpdate(prevProps: any, prevState: any) {
        if (this.state.isLoaded === true) {
            return;
        }
        console.log(`UserForm componentDidUpdate`);
        this.loadUserItems();
    }
}

export default UserForm;
