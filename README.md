# React.js based frontend for the future ark

This repository is a basic application able to read and edit a postgresql datababse from a react.js frontend.

It has three components:
* postgresql database running on port 5432
* graphql backend server connected to the above mentionned database
* typescipt-react.js frontend

This README.md file will walk you through all the steps to run these three components.

## Before starting

First of all install the npm packages

```bash
npm install
```

### Creating the database (only the first time)

Run the database:

```bash
docker-compose up db
```

Using any postgresql database client that you want create a database named "greensteps"

### Generate the database from the dbdiagram

Go to https://dbdiagram.io/d/5f06d29a0425da461f04729d

Menu Export > Export PostgreSQL. Download the file in this project folder and name it:  "greensteps"

Copy the content to the clipboard.

Then on the database client open a script editor on the "greensteps" database and paste the script that you just copied.

#### Using psql 

1. Connect to the psql client 

```bash
docker-compose run db psql -U odoo -h db -d postgres # the password is "odoo"
```

2. Create a database named "greensteps"

```sql
CREATE DATABASE greensteps;
```

3. Select the database you just created

```sql
\c greensteps
```

4. Run the table scripts
```sql
<paste the content of the script you copied from dbdiagram here>
```

5. Quit the client

```sql
\q
```


**Remark**: you can also use the content of the hackathon.sql file saved in this repository.

It should create a few tables in your database:
* event
* event_participant
* user

At this point you are done creating the initial database.

## Running the backend

Warning: if you did not create the database following the steps from the previous section you cannot move forward on this section.

Running the backend is necessary every time you start working on this project.

There are only two steps.

**Make sure the database is running**:

```bash
docker-compose up db
```

Wait a minute until the database is up and running then:

**Run the postgraphile server**:

```bash
docker-compose up postgraphile
```

At this point you can already connect to the PostGraphiQL server running on [localhost:5000](http://localhost:5000/)

## Running the frontend

At this point, when the backend is already running you can just start the project like any react application:

```bash
npm start
```