CREATE TABLE "event" (
  "id" SERIAL UNIQUE PRIMARY KEY,
  "created_at" timestamp,
  "modified_at" timestamp,
  "start_time" timestamp,
  "end_time" timestamp,
  "name" varchar,
  "description" varchar,
  "location" varchar
);

CREATE TABLE "user" (
  "id" SERIAL UNIQUE PRIMARY KEY,
  "full_name" varchar,
  "created_at" timestamp,
  "country_code" int,
  "birth_date" timestamp
);

CREATE TABLE "event_participant" (
  "id" SERIAL UNIQUE PRIMARY KEY,
  "user_id" int,
  "event_id" int
);

ALTER TABLE "event_participant" ADD FOREIGN KEY ("user_id") REFERENCES "user" ("id");

ALTER TABLE "event_participant" ADD FOREIGN KEY ("event_id") REFERENCES "event" ("id");

COMMENT ON TABLE "user" IS 'Stores user data';

COMMENT ON COLUMN "user"."full_name" IS 'Full name';
